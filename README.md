sql table:-
1. users - id,name,phoneno
2. locations-id,name
3. status-id,name
4. drivers -id,name,phoneno,capno,currentLocation
5. rides- id, userid,driverid,sourcedestinationId, statusid
6. sourcerdestinationDistance -id,sourceid,destinationid,distance,price

stored procedure:
    -usp_GetDrivers

endpoints:

places:
       1. locations
            -get all locations
       2. locations/:locationid
            -get location by id
users:
      1. users
           -get all users
      2. users/:userId
            -get user by id
      3. users/:userid
            -delete user by id
      4. post,put methods alse
drivers:
       drivers
          -get all drivers
       drivers/:driverId
           -get driver by id
       drivers/locations/:sourceId
            -get drivers by sourceid which are at near 5kms
   
history:
       user/:userid/rides
           -get user rides by user id
       driver/:driverId/rides
           -get driver rides by driver id

