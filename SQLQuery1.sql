CREATE DATABASE TrackX

USE TrackX

CREATE TABLE Users (
	Id INT PRIMARY KEY IDENTITY(1, 1)
	,Name VARCHAR(200) NOT NULL
	,PhoneNumber VARCHAR(25) NOT NULL
	)

INSERT INTO Users
VALUES (
	'user1'
	,'8754534545'
	)
	,(
	'user2'
	,'8534059324'
	)
	,(
	'user3'
	,'4234279098'
	)

CREATE TABLE Statuses (
	Id INT PRIMARY KEY IDENTITY(1, 1)
	,Name VARCHAR(50) NOT NULL
	)

INSERT INTO Statuses
VALUES ('normal')
	,('completed')
	,('pending')
	,('onride')
	,('cancelled')
	,('confirmed')

CREATE TABLE Locations (
	Id INT PRIMARY KEY IDENTITY(1, 1)
	,Name VARCHAR(300) NOT NULL
	)

INSERT INTO Locations
VALUES ('hyderabad')
	,('eluru')
	,('kakinada')
	,('Us')

CREATE TABLE Drivers (
	Id INT PRIMARY KEY IDENTITY(1, 1)
	,Name VARCHAR(200) NOT NULL
	,PhoneNumber VARCHAR(25) NOT NULL
	,VechicleNumber VARCHAR(45) NOT NULL
	,CurrentLocationId INT NOT NULL
	,FOREIGN KEY (CurrentLocationId) REFERENCES Locations(id)
	)

INSERT INTO Drivers
VALUES (
	'sai mahesh'
	,'875456445'
	,'ap27aw2145'
	,2
	)
	,(
	'sai'
	,'9999999999'
	,'ap27aw2134'
	,1
	)
	,(
	'mahesh'
	,'888888888'
	,'ap24ae2346'
	,2
	)

SELECT *
FROM DRIVERS

CREATE TABLE SourceToDestinationDistances (
	Id INT PRIMARY KEY IDENTITY(1, 1)
	,SourceId INT NOT NULL
	,DestinationId INT NOT NULL
	,Distance INT NOT NULL
	,Price INT NOT NULL FOREIGN KEY (SourceId) REFERENCES Locations(id)
	,FOREIGN KEY (DestinationId) REFERENCES Locations(id)
	)

INSERT INTO SourceToDestinationDistances
VALUES (
	1
	,2
	,5
	,199
	)
	,(
	1
	,3
	,10
	,250
	)
	,(
	2
	,3
	,20
	,450
	)

CREATE TABLE Rides (
	Id INT PRIMARY KEY IDENTITY(1, 1)
	,UserId INT NOT NULL
	,DriverId INT NOT NULL
	,StatusId INT NOT NULL
	,SourceDetinationId INT NULL
	,FOREIGN KEY (UserId) REFERENCES Users(id)
	,FOREIGN KEY (DriverId) REFERENCES Drivers(id)
	,FOREIGN KEY (StatusId) REFERENCES Statuses(id)
	,FOREIGN KEY (SourceDetinationId) REFERENCES SourceToDestinationDistances(id)
	)

SELECT * 
FROM Rides 
WHERE UserId=1


INSERT INTO Rides
VALUES (
	1
	,1
	,5
	,1
	)
	,(
	1
	,2
	,3
	,2
	)
--to get drivers near 5kms (drivers/locations/id)
GO
CREATE PROCEDURE usp_GetRiders (@sourceId INT)
AS
SELECT D.Id
	,D.Name
	,D.PhoneNumber
	,D.VechicleNumber
FROM Drivers D INNER JOIN
 (
		SELECT DestinationId AS Id
		FROM SourceToDestinationDistances S
		WHERE s.SourceId = @sourceId
			AND S.Distance <= 5
		) AS FS
	ON D.CurrentLocationId = FS.Id
	 
EXEC usp_GetRiders 1

