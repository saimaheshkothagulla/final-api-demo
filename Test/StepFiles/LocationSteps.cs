﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using TechTalk.SpecFlow;
using Test.MockResources;

namespace Test.StepFiles
{   
    [Scope(Feature = "Location Resource")]
    [Binding]
    public class LocationSteps : BaseSteps
    {
        public LocationSteps(CustomWebApplicationFactory factory)
           : base(factory.WithWebHostBuilder(builder =>
           {
               builder.ConfigureServices(services =>
               {
                    // Mock Repo
                    services.AddScoped(_ => LocationMock.LocationRepositoryMock.Object);
               });
           }))
        {
        }

        [BeforeScenario]
        public static void Mocks()
        {
            LocationMock.MockLocationRepositoryImplementation();

        }
    }
}
