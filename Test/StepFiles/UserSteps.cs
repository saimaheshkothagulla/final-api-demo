﻿using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Testing;
using TechTalk.SpecFlow;
using TrackX;
using Xunit;
using Microsoft.Extensions.DependencyInjection;
using Test.MockResources;
namespace Test.StepFiles
{
    [Scope(Feature = "User Resource")]
    [Binding]
    public class UserSteps : BaseSteps
    {
        public UserSteps(CustomWebApplicationFactory factory)
           : base(factory.WithWebHostBuilder(builder =>
           {
               builder.ConfigureServices(services =>
               {
                   // Mock Repo
                   services.AddScoped(_ => UserMock.UserRepositoryMock.Object);
               });
           }))
        {
        }

        [BeforeScenario]
        public static void Mocks()
        {
            UserMock.MockUserRepositoryImplementation();

        }
    }
}
