﻿using Microsoft.Extensions.DependencyInjection;
using TechTalk.SpecFlow;
using Test.MockResources;
namespace Test.StepFiles
{
    [Scope(Feature = "Ride Resource")]
    [Binding]
    public class RideSteps :BaseSteps
    {
        public RideSteps(CustomWebApplicationFactory factory)
          : base(factory.WithWebHostBuilder(builder =>
          {
              builder.ConfigureServices(services =>
              {
                   // Mock Repo
                   services.AddScoped(_ => RideMock.RideRepositoryMock.Object);
              });
          }))
        {
        }

        [BeforeScenario]
        public static void Mocks()
        {
            RideMock.MockRideRepositoryImplementation();

        }
    }
}
