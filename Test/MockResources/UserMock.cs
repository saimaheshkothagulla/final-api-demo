﻿using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrackX.Models.DB;
using TrackX.Models.Request;
using TrackX.Repositories;

namespace Test.MockResources
{
    public class UserMock
    {
        public static readonly Mock<IUserRepository> UserRepositoryMock = new Mock<IUserRepository>();
        private static readonly IEnumerable<User> ListOfUsers = new List<User>
        {
            new User
            {
                Id = 1,
                Name = "user1",
                PhoneNumber = "9999999999"
            },
            new User
            {
                Id = 2,
                Name = "user2",
                PhoneNumber = "8888888888"
            }

        };
        public static void MockUserRepositoryImplementation()
        {
            UserRepositoryMock.Setup(x => x.GetAll())
                    .Returns(ListOfUsers);
            UserRepositoryMock.Setup(x => x.Get(It.IsAny<int>()))
                    .Returns((int id) =>
                    ListOfUsers.SingleOrDefault(x => x.Id == id)
                    );
            UserRepositoryMock.Setup(x => x.Add(It.IsAny<UserRequest>()));
            UserRepositoryMock.Setup(x => x.Update(It.IsAny<UserRequest>(), It.IsAny<int>()));
            UserRepositoryMock.Setup(x => x.Delete(It.IsAny<int>()));

        }
    }
}
