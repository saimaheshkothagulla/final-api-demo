﻿using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrackX.Models.DB;
using TrackX.Repositories;
namespace Test.MockResources
{
    public class RideMock
    {
        public static readonly Mock<IRideRepository> RideRepositoryMock = new Mock<IRideRepository>();
        private static readonly IEnumerable<Ride> ListOfRides = new List<Ride>
        {
            new Ride
            {
                Id = 1,
                UserId=1,
                DriverId=2,
                StatusId=1,
                SourceDetinationId=1
            },
            new Ride
            {
                Id = 2,
                UserId=2,
                DriverId=1,
                StatusId=2,
                SourceDetinationId=2

            }

        };
        public static void MockRideRepositoryImplementation()
        {
            RideRepositoryMock.Setup(x => x.GetUserHistory(It.IsAny<int>()))
                    .Returns((int id) =>
                    ListOfRides.Where(r=>r.UserId==id)
                    );
            RideRepositoryMock.Setup(x => x.GetDriverHistory(It.IsAny<int>()))
                    .Returns((int id) =>
                    ListOfRides.Where(r => r.DriverId == id)
                    );
        }

    }
}
