﻿using Moq;
using System.Collections.Generic;
using System.Linq;
using TrackX.Models.Response;
using TrackX.Repositories;

namespace Test.MockResources
{
    public class LocationMock
    {
        public static readonly Mock<ILocationRepository> LocationRepositoryMock = new Mock<ILocationRepository>();
        private static readonly IEnumerable<LocationResponse> ListOfLocations = new List<LocationResponse>
        {
            new LocationResponse
            {
                Id = 1,
                Name = "Location1"
            },
            new LocationResponse
            {
                Id = 2,
                Name = "Location2"
            }

        };
        public static void MockLocationRepositoryImplementation()
        {
            LocationRepositoryMock.Setup(x => x.GetAll())
                    .Returns(ListOfLocations);
            LocationRepositoryMock.Setup(x => x.Get(It.IsAny<int>()))
                    .Returns((int id) =>
                    ListOfLocations.SingleOrDefault(x => x.Id == id)
                    );

        }


    }
}
