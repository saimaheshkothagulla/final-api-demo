﻿using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrackX.Models.Response;
using TrackX.Repositories;

namespace Test.MockResources
{
    public class DriverMock
    {
        public static readonly Mock<IDriverRepository> DriverRepositoryMock = new Mock<IDriverRepository>();
        private static readonly IEnumerable<DriverResponse> ListOfDrivers = new List<DriverResponse>
        {
            new DriverResponse
            {
                Id = 1,
                Name = "driver1",
                PhoneNumber = "9999999999",
                VechicleNumber="ap27aw1234"
            },
            new DriverResponse
            {
                Id = 2,
                Name = "driver2",
                PhoneNumber = "8888888888",
                VechicleNumber="ap27aw4321"
            }

        };
        public static void MockDriverRepositoryImplementation()
        {
            DriverRepositoryMock.Setup(x => x.GetAll())
                    .Returns(ListOfDrivers);
            DriverRepositoryMock.Setup(x => x.Get(It.IsAny<int>()))
                    .Returns((int id) =>
                    ListOfDrivers.SingleOrDefault(x => x.Id == id)
                    );
            DriverRepositoryMock.Setup(x => x.GetLocationId(It.IsAny<int>()))
               .Returns((int id) => {
                   var driversId = getDrivers(id);
                   if (driversId == null)
                   {
                       return null;
                   }
                   else
                   {
                       var drivers = ListOfDrivers.Where(a => driversId.Contains(a.Id));
                       return drivers;

                   }

               });

        }
        public static List<int> getDrivers(int id)
        {
            var LocationDriversDictionary = new Dictionary<int, List<int>>();
            var ListOfDriversId = new List<int>() { 1 };
            LocationDriversDictionary.Add(1, ListOfDriversId);
            var result = LocationDriversDictionary.ContainsKey(id) ? LocationDriversDictionary[id] : null;
            return result;
        }
    }
}
