﻿Feature: Ride Resource

Scenario: Get Rides By User Id
	Given I am a client
	When I make GET Request '/users/1/rides'
	Then response code must be '200'
	And response data must look like '[{"id":1,"userId":1,"driverId":2,"statusId":1,"sourceDetinationId":1}]'

	Scenario: Get Rides By Driver Id
	Given I am a client
	When I make GET Request '/drivers/1/rides'
	Then response code must be '200'
	And response data must look like '[{"id":2,"userId":2,"driverId":1,"statusId":2,"sourceDetinationId":2}]'
