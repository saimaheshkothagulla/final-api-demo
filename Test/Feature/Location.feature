﻿Feature: Location Resource

Scenario: Get Locations All
	Given I am a client
	When I make GET Request '/locations'
	Then response code must be '200'
	And response data must look like '[{"id":1,"name":"Location1"},{"id":2,"name":"Location2"}]'

Scenario: Get Location By Id
	Given I am a client
	When I make GET Request '/locations/2'
	Then response code must be '200'
	And response data must look like '{"id":2,"name":"Location2"}'

Scenario: Get Location By Wrong Id
	Given I am a client
	When I make GET Request '/locations/8'
	Then response code must be '404'
	And response data must look like 'Invalid location Id..'