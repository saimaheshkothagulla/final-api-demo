﻿Feature: User Resource

Scenario: Get Users All
	Given I am a client
	When I make GET Request '/users'
	Then response code must be '200'
	And response data must look like '[{"id":1,"name":"user1","phoneNumber":"9999999999"},{"id":2,"name":"user2","phoneNumber":"8888888888"}]'

Scenario: Get User By Id
	Given I am a client
	When I make GET Request '/users/1'
	Then response code must be '200'
	And response data must look like '{"id":1,"name":"user1","phoneNumber":"9999999999"}'

Scenario: Get User By Wrong Id
	Given I am a client
	When I make GET Request '/users/8'
	Then response code must be '404'
	And response data must look like 'Invalid User Id..'

Scenario: Create An User
	Given I am a client
	When I am making a post request to '/users' with the following Data '{"name":"user1","phoneNumber":"9999999999"}'
	Then response code must be '200'

Scenario: Update User By User Id
	Given I am a client
	When I make PUT Request '/users/1' with the following Data with the following Data '{"name":"user1","phoneNumber":"9999999999"}'
	Then response code must be '200'
	And response data must look like 'Successfully upadated..'


Scenario: Update User By Wrong Id
	Given I am a client
	When I make PUT Request '/users/10' with the following Data with the following Data '{"name":"user1","phoneNumber":"9999999999"}'
	Then response code must be '404'

Scenario:Delete User By Id
	Given I am a client
	When I make Delete Request 'users/1'
	Then response code must be '200'

Scenario: Delete User By Wrong Id
	Given I am a client
	When I make Delete Request 'users/8'
	Then response code must be '404'
	And response data must look like 'Invalid user id'


