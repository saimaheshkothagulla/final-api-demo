﻿Feature: Driver Resource

Scenario: Get Drivers All
	Given I am a client
	When I make GET Request '/drivers'
	Then response code must be '200'
	And response data must look like '[{"id":1,"name":"driver1","phoneNumber":"9999999999","vechicleNumber":"ap27aw1234"},{"id":2,"name":"driver2","phoneNumber":"8888888888","vechicleNumber":"ap27aw4321"}]'

Scenario: Get Driver By Id
	Given I am a client
	When I make GET Request '/drivers/1'
	Then response code must be '200'
	And response data must look like '{"id":1,"name":"driver1","phoneNumber":"9999999999","vechicleNumber":"ap27aw1234"}'

Scenario: Get Driver By Wrong Id
	Given I am a client
	When I make GET Request '/drivers/8'
	Then response code must be '404'
	And response data must look like 'Invalid driver Id..'


Scenario: Get Drivers By Location Id
	Given I am a client
	When I make GET Request '/drivers/locations/1'
	Then response code must be '200'
	And response data must look like '[{"id":1,"name":"driver1","phoneNumber":"9999999999","vechicleNumber":"ap27aw1234"}]'
