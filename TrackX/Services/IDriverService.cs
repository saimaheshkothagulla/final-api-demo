﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TrackX.Models.Response;

namespace TrackX.Services
{
    public interface IDriverService
    {
        public IEnumerable<DriverResponse> GetAll();
        public DriverResponse Get(int id);
        public IEnumerable<DriverResponse> GetLocationId(int id);
    }
}
