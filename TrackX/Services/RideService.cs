﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TrackX.Models.DB;
using TrackX.Repositories;

namespace TrackX.Services
{
    public class RideService : IRideService
    {
        private readonly IRideRepository _rideRepository;
        public RideService(IRideRepository rideRepository)
        {
            _rideRepository= rideRepository;
        }
        public IEnumerable<Ride> GetDriverHistory(int driverId)
        {
            var rides = _rideRepository.GetDriverHistory(driverId);
            return rides;
        }

        public IEnumerable<Ride> GetUserHistory(int userId)
        {
            var rides = _rideRepository.GetUserHistory(userId);
            return rides;
        }
    }
}
