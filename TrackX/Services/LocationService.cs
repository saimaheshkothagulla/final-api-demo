﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TrackX.Models.Response;
using TrackX.Repositories;

namespace TrackX.Services
{
    public class LocationService : ILocationService
    {
        private readonly ILocationRepository _locationRepository;
        public LocationService(ILocationRepository locationRepositor)
        {
            _locationRepository = locationRepositor;
        }
        public LocationResponse Get(int id)
        {

            var location = _locationRepository.Get(id);
            if (location != null)
            {
                return location;
            }
            throw new Exception();
        }

        public IEnumerable<LocationResponse> GetAll()
        {
            return _locationRepository.GetAll();
        }
    }
}
