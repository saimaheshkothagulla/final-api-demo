﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TrackX.Models.Response;
using TrackX.Repositories;

namespace TrackX.Services
{
    public class DriverService : IDriverService
    {
        private readonly IDriverRepository _driverRepository;


        public DriverService(IDriverRepository driverRepository)
        {
            _driverRepository = driverRepository;
        }
        public DriverResponse Get(int id)
        {
            var driver = _driverRepository.Get(id);
            if (driver != null)
            {
                return driver;
            }
            throw new Exception();
        }

        public IEnumerable<DriverResponse> GetAll()
        {
            return _driverRepository.GetAll();
        }

        public IEnumerable<DriverResponse> GetLocationId(int id)
        {
            var drivers = _driverRepository.GetLocationId(id);
            if (drivers.Count() == 0)
            {
                throw new Exception();
            }
            return drivers;
        }
    }
}
