﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TrackX.Models.DB;
using TrackX.Models.Request;
using TrackX.Repositories;

namespace TrackX.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }
        public void Add(UserRequest user)
        {
            _userRepository.Add(user);
        }

        public void Delete(int id)
        {
            var userById = _userRepository.Get(id);
            if (userById != null)
            {
                _userRepository.Delete(id);
            }
            else
            {
                throw new Exception();
            }
        }

        public User Get(int id)
        {
            var user = _userRepository.Get(id);
            if (user != null)
            {
                return user;
            }
            throw new Exception();
        }

        public IEnumerable<User> GetAll()
        {
            var users = _userRepository.GetAll();
            return users;
        }

        public void Update(UserRequest user, int id)
        {
            var userById = _userRepository.Get(id);
            if (userById != null)
            {
                _userRepository.Update(user, id);
            }
            else
            {
                throw new Exception();
            }
        }
    }
}
