﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TrackX.Models.Response;

namespace TrackX.Services
{
    public interface ILocationService
    {
        public IEnumerable<LocationResponse> GetAll();
        public LocationResponse Get(int id);
    }
}
