﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TrackX.Services;

namespace TrackX.Controllers
{
    [Route("drivers")]
    [ApiController]
    public class DriversController : ControllerBase
    {
        private readonly IDriverService _driverService;

        public DriversController(IDriverService driverService)
        {
            _driverService = driverService;
        }
        [HttpGet]
        public IActionResult GetAll()
        {
            return Ok(_driverService.GetAll());
        }

        // GET api/<LocationController>/5
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            try
            {
                var driver = _driverService.Get(id);
                return Ok(driver);
            }
            catch (Exception)
            {
                return NotFound("Invalid driver Id..");
            }

        }
        [HttpGet("locations/{sourceId}")]
        public IActionResult GetByLocationId(int sourceId)
        {
            try
            {
                var drivers = _driverService.GetLocationId(sourceId);
                return Ok(drivers);
            }
            catch (Exception)
            {
                return NotFound("Invalid source Id..");
            }

        }
    }
}
