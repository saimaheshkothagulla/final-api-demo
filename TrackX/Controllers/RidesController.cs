﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TrackX.Services;

namespace TrackX.Controllers
{
    [ApiController]
    public class RidesController : ControllerBase
    {
        private readonly IRideService _rideService;

        public RidesController(IRideService rideService)
        {
            _rideService = rideService;
        }
        [HttpGet("users/{id}/rides")]
           public IActionResult GetuserHistory(int id)
        {
            try
            {
                var rides = _rideService.GetUserHistory(id);
                return Ok(rides);
            }
            catch (Exception)
            {
                return NotFound("invalid user id");
            }


        }
        [HttpGet("drivers/{id}/rides")]
        public IActionResult GetDriverHistory(int id)
        {
            try
            {
                var rides = _rideService.GetDriverHistory(id);
                return Ok(rides);
            }
            catch (Exception)
            {
                return NotFound("invalid driver id");
            }

        }
    }
}
