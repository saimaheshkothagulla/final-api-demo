﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TrackX.Services;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace TrackX.Controllers
{
    [Route("locations")]
    [ApiController]
    public class LocationsController : ControllerBase
    {
        private readonly ILocationService _locationService;

        public LocationsController(ILocationService locationService)
        {
            _locationService = locationService;
        }
        // GET: api/<LocationController>
        [HttpGet]
        public IActionResult GetAll()
        {
            return Ok(_locationService.GetAll());
        }

        // GET api/<LocationController>/5
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            try
            {
                var location = _locationService.Get(id);
                return Ok(location);
            }
            catch (Exception)
            {
                return NotFound("Invalid location Id..");
            }

        }
    }
}
