﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TrackX.Models.Request
{
    public class UserRequest
    {
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
    }
}
