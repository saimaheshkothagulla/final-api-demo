﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TrackX.Models.Request
{
    public class DriverRequest
    {
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public string VechicleNumber { get; set; }
        public string CurrentLocationId { get; set; }
    }
}
