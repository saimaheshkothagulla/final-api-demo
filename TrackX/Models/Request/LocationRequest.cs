﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TrackX.Models.Request
{
    public class LocationRequest
    {
        public string Name { get; set; }
    }
}
