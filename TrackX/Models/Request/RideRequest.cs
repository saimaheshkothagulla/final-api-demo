﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TrackX.Models.Request
{
    public class RideRequest
    {
        public int UserId { get; set; }
        public int DriverId { get; set; }
        public int StatusId { get; set; }
        public int SourceDetinationId { get; set; }
    }
}
