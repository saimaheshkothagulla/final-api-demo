﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TrackX.Models.DB;
using TrackX.Models.Request;

namespace TrackX.Repositories
{
    public interface IUserRepository
    {
        public IEnumerable<User> GetAll();
        public User Get(int id);
        public void Add(UserRequest user);
        public void Update(UserRequest user, int id);
        public void Delete(int id);
    }
}
