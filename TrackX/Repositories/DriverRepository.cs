﻿using Dapper;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using TrackX.Models.Request;
using TrackX.Models.Response;

namespace TrackX.Repositories
{
    public class DriverRepository : IDriverRepository
    {

        private readonly ConnectionString _connection;

        public DriverRepository(IOptions<ConnectionString> connection)
        {
            _connection = connection.Value;
        }
        public DriverResponse Get(int id)
        {
            string sql = @"SELECT Id,Name,PhoneNumber,VechicleNumber
                           FROM Drivers WHERE Id= @id";

            using (var connection = new SqlConnection(_connection.DB))
            {
                var driver = connection.QueryFirst<DriverResponse>(sql, new { id = id });
                return driver;
            }
        }

        public IEnumerable<DriverResponse> GetAll()
        {
            string sql = @"SELECT Id,Name,PhoneNumber,VechicleNumber
                           FROM Drivers";

            using (var connection = new SqlConnection(_connection.DB))
            {
                var drivers = connection.Query<DriverResponse>(sql);
                return drivers;
            }
        }

        public IEnumerable<DriverResponse> GetLocationId(int id)
        {
            string sql = "usp_GetDrivers @id";
            using (var connection = new SqlConnection(_connection.DB))
            {
                var drivers = connection.Query<DriverResponse>(sql, new { id = id });
                return drivers;
            }
        }

    }
}

