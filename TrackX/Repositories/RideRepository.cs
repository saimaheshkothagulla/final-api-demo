﻿using Dapper;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using TrackX.Models.DB;
using TrackX.Models.Response;

namespace TrackX.Repositories
{
    public class RideRepository : IRideRepository
    {

        private readonly ConnectionString _connection;

        public RideRepository(IOptions<ConnectionString> connection)
        {
            _connection = connection.Value;
        }
        public IEnumerable<Ride> GetDriverHistory(int driverId)
        {
            string sql = @"SELECT * 
                            FROM Rides 
                            WHERE DriverId= @driverId";

            using (var connection = new SqlConnection(_connection.DB))
            {
                var rides = connection.Query<Ride>(sql,new { driverId=driverId });
                return rides;
            }

        }

        public IEnumerable<Ride> GetUserHistory(int userId)
        {
            string sql = @"SELECT * 
                            FROM Rides 
                            WHERE UserId= @userId";

            using (var connection = new SqlConnection(_connection.DB))
            {
                var rides = connection.Query<Ride>(sql, new { userId = userId });
                return rides;
            }

        }
    }
}
