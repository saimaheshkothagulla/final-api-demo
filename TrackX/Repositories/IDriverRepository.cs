﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TrackX.Models.Request;
using TrackX.Models.Response;

namespace TrackX.Repositories
{
    public interface IDriverRepository
    {
        public IEnumerable<DriverResponse> GetAll();
        public DriverResponse Get(int id);
        public IEnumerable<DriverResponse> GetLocationId(int id);
    }
}
