﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TrackX.Models.DB;

namespace TrackX.Repositories
{
    public interface IRideRepository
    {
        public IEnumerable<Ride> GetUserHistory(int userId);
        public IEnumerable<Ride> GetDriverHistory(int driverId);
    }
}
