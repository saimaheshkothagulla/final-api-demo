﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TrackX.Models.Response;

namespace TrackX.Repositories
{
    public interface ILocationRepository
    {
        public IEnumerable<LocationResponse> GetAll();
        public LocationResponse Get(int id);
    }
}
