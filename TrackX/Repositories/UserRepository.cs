﻿using Dapper;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using TrackX.Models.DB;
using TrackX.Models.Request;
using TrackX.Models.Response;

namespace TrackX.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly ConnectionString _connection;

        public UserRepository(IOptions<ConnectionString> connection)
        {
            _connection = connection.Value;
        }
        public void Add(UserRequest user)
        {
            string sql = @"INSERT INTO Users
                            VALUES (
	                            @name
	                            ,@phoneno
	                            )";
            using (var connection = new SqlConnection(_connection.DB))
            {
                connection.Query<UserResponse>(sql, new { name =user.Name,phoneno= user.PhoneNumber });

            }
        }

        public void Delete(int id)
        {
            string sql = @"
                            DELETE
                            FROM Rides
                            WHERE UserId = @id

                            DELETE
                            FROM Users
                            WHERE Id = @id";
            using (var connection = new SqlConnection(_connection.DB))
            {
                connection.Query<UserResponse>(sql, new { id = id });

            }
        }

        public User Get(int id)
        {
            string sql = @"SELECT *
                        FROM Users
                        WHERE Id = @id";
            using (var connection = new SqlConnection(_connection.DB))
            {
                var user = connection.QueryFirst<User>(sql, new { id = id });
                return user;
            }
        }

        public IEnumerable<User> GetAll()
        {
            string sql = @"SELECT *
                           FROM Users";

            using (var connection = new SqlConnection(_connection.DB))
            {
                var users = connection.Query<User>(sql);
                return users;
            }
        }

        public void Update(UserRequest user, int id)
        {
            string sql = @"UPDATE Users
                            SET Name = @name
	                            ,PhoneNumber=@phoneno
                            WHERE Id = @id";
            using (var connection = new SqlConnection(_connection.DB))
            {
                connection.Query<UserResponse>(sql, new { name = user.Name, phoneno = user.PhoneNumber , id = id });

            }
        }
    }
}
