﻿using Dapper;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using TrackX.Models.Response;

namespace TrackX.Repositories
{
    public class LocationRepository : ILocationRepository
    {
        private readonly ConnectionString _connection;

        public LocationRepository(IOptions<ConnectionString> connection)
        {
            _connection = connection.Value;
        }
        public LocationResponse Get(int id)
        {
            string sql = @"SELECT *
                           FROM Locations WHERE Id= @id";

            using (var connection = new SqlConnection(_connection.DB))
            {
                var location = connection.QueryFirst<LocationResponse>(sql, new { id = id });
                return location;
            }
        }

        public IEnumerable<LocationResponse> GetAll()
        {
            string sql = @"SELECT *
                           FROM Locations";

            using (var connection = new SqlConnection(_connection.DB))
            {
                var locations = connection.Query<LocationResponse>(sql);
                return locations;
            }
        }
    }
}
